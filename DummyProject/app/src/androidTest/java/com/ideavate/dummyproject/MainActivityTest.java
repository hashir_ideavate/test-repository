package com.ideavate.dummyproject;

import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.action.ViewActions;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by hashir on 23/5/16.
 */

@RunWith(AndroidJUnit4.class)
@SmallTest
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {

    private MainActivity mActivity;

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Before
    public void setUp() throws Exception {
        super.setUp();
        injectInstrumentation(InstrumentationRegistry.getInstrumentation());
        mActivity = getActivity();
    }

    @Test
    public void checkFirstText(){
        onView(withId(R.id.first)).check(matches(Utils.withHint("First Number")));

    }


    @Test
    public void checkSecondText(){
        onView(withId(R.id.second)).check(matches(Utils.withHint("Second Number")));

    }


    @Test
    public void checkButtonText(){
        onView(withId(R.id.calculator)).check(matches(withText("ADD")));

    }

    @Test
    public void checkTotal(){

                onView(withId(R.id.first)).perform(ViewActions.typeText("25"));
                onView(withId(R.id.second)).perform(ViewActions.typeText("26"));
                onView(withId(R.id.calculator)).perform(ViewActions.click());
                onView(withId(R.id.total)).check(matches(withText("51")));

    }

}
