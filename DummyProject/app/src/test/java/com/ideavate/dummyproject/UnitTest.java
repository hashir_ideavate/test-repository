package com.ideavate.dummyproject;

import android.content.Context;

import org.junit.Test;
import org.mockito.Mock;

import static junit.framework.Assert.assertEquals;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class UnitTest {

    @Mock
    Context mMockContext;

    @Mock
    MainActivity mMainActivity;

    @Test
    public void nullCheckForShowToast() {
        Exception ex = null;
        try {
            mMainActivity.showToast(null);
        } catch (Exception e) {
            ex = e;
        }
        assertEquals(null, ex);
    }

    @Test
    public void checkSum() {
        Exception ex = null;
        String total = null;
        try {
            total = mMainActivity.getTotal(2,4);
        } catch (Exception e) {
            ex = e;
        }
        assertEquals(null, ex);
        assertEquals("6", total);
    }

}